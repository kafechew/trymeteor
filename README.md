# Companion to [Try Meteor](https://www.meteor.com/try) #

The Meteor official tutorial, "Try Meteor" is the best way to start learn about Meteor, however, some concepts are new to beginners, especially those without programming experience.

### What is this repository for? ###

* Anatomy of the code of Try Meteor
* Terms and Concepts of Meteor

### [0. Installing Meteor](https://www.meteor.com/install) ###

Straightforward.

### [1. Creating an app](https://www.meteor.com/try) ###

Straightforward.

### [2. Templates](https://www.meteor.com/try/2) ###

Inclusions/Template {{> templateName}} 

Expressions/Values {{valueData}}

Block helpers/Functions {{#helper arrayName}} {{/helper}} - think of logic

### [3. Collections](https://www.meteor.com/try/3) ###

* Create a new Collection, in this case "Tasks" collection.
* Insert data into the collection through client and server
* Retrive data from mongodb and view on web client

### [4. Forms & Events](https://www.meteor.com/try/4) ###
* Insert data through web client form

### [5. Update and remove](https://www.meteor.com/try/5) ###
* Check buttons to true/false and delete tasks